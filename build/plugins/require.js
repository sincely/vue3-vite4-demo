import commonjs from 'vite-plugin-commonjs'

export default function commonjsPlugin() {
  return commonjs()
}
